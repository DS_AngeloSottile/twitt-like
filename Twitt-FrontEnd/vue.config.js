module.exports =
{
    devServer:
    {
        proxy: 'http://localhost:5000',
        port: 5000,
    },
    css:
    {
        loaderOptions:
        {
            sass:
            {
                prependData:    `@import "@/assets/scss/_variables.scss";
                                @import "@/assets/scss/app.scss";`
            }
        }
    } 
};