import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuelidate from 'vuelidate' //importo vueValidate
import axios from 'axios'
import '@/assets/scss/tailwind.scss'

Vue.config.productionTip = false


    
//configurazione di axios per le chiamata api
axios.defaults.baseURL ='http://localhost:8000/api/';
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';


axios.interceptors.request.use( config => 
{
  const storeToken = store.getters.token;

  if(storeToken)
  {
    config.headers.Authorization = `Bearer ${storeToken}`;
  }
  return config;
});

axios.interceptors.response.use( config => 
{
  return config;
});
//configurazione di axios per le chiamata api



Vue.use(Vuelidate); //utilizzo di vueValidate

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
