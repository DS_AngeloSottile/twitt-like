import axios from 'axios';
import router from '../../router'
const state = 
{
    posts: null,
    errorsMyPosts: null, 
    myPostsFuture: null,
    myPublishedPosts: null,
    singlePost: null,
    postData: {},
    errors: {},
};

const getters = 
{   //getter per prendere tutti i post
    posts: state =>
    {
        return state.posts;
    },
    //getter per prendere i post futuri dal singolo utente
    myPostsFuture: state =>
    {
        return state.myPostsFuture;
    },

    // getter per prendere i post pubblicati dal singolo utente
    myPublishedPosts: state =>
    {
        return state.myPublishedPosts;
    },
    
    // getter per prendere un post singolarmente
    singlePost: state =>
    {
        
        return state.singlePost;
    },
    
    //getter per prendere i dati del nuovo post appena creato
    postData: state =>
    {
        return state.postData;
    },

    errors: state =>
    {
        return state.errors;
    },
    errorsMyPosts: state =>
    {
        return state.errorsMyPosts;
    },
};

const mutations =
{   //mutation per tutti post
    setAllPosts: ( state,payload) => // assegno al valore dello state quello della chiamata api
    {
        state.posts = payload;
    },
    //mutation per tutti i post PUBBLICATI dal singolo utente
    setMyPublishedPosts: ( state,payload) =>  // assegno al valore dello state quello della chiamata api
    {
        state.myPublishedPosts = payload;
    },
    //mutation per tutti i post FUTURI del singolo utente
    setMyPostsFuture: ( state,payload) =>  // assegno al valore dello state quello della chiamata api
    {
        state.myPostsFuture = payload;
    },
    //mutations per prendere un singolo post (verrà richiamato nella view singlePost per visualizzare l'intero post da modificare)
    setSinglePost: ( state,payload) =>  // assegno al valore dello state quello della chiamata api
    {
        state.singlePost = payload;
    },
    // mutation che memorizza nello state i dati del post appena creato
    setPostData: ( state,payload) =>  // assegno al valore dello state quello della chiamata api
    {
        state.postData = payload; 
    },
    
    setErrors: ( state,payload) =>  
    {
        state.errors = payload; 
    },
    setErrorsMyPosts: ( state,payload) =>  
    {
        state.errorsMyPosts = payload; 
    },
    
    resetErrors: (state) =>
    {
        state.errors = {};
    },
};

const actions =
{ 
    //chiamata api per prndere tutti i post PUBBLICATI
    getAllPosts: ( {commit} ) =>
    {
        axios
        .get('posts') //route api laravel
        .then(res => 
            {
                console.log(res);
                commit('setAllPosts', res.data.result); // richiamo setAllPost passando tutti i post ricevuti da laravel
            })
        .catch( (err) =>
            {
                console.log(err);
            });

    },

    //chiamata api per prendere tutti i post pubblicati dal singolo utente
    getAllMyPublishedPosts: ( {commit} ) =>
    {
        axios
        .post('my-postPublished') // rotta laravel
        .then(res => 
            {
                commit('setMyPublishedPosts', res.data.result); //richiamo setMyPostPubblicati passanto tutti i post pubblicati dal singolo utente ricevuti la laravel
            })
        .catch( (err) =>
            {
                commit("setErrorsMyPosts",err.response.data.result);
            });

    },

    //chiamata api per prendere i postda pubblicare per ogni utente
    getAllMyFuturePosts: ( {commit} ) =>
    {
        axios
        .post('my-postFuture') // rotta laravel
        .then(res => 
            {
                commit('setMyPostsFuture', res.data.result); //richiamo la funzione setMyPostFuturi passando i post da pubblicare del singolo utente tramite laravel
            })
        .catch( (err) =>
            {
                commit("setErrorsMyPosts",err.response.data.result);
            });

    },

    //chiamata api che recupera il singolo post riconoscendolo tramite id
    getSinglePost: ( {commit},payload ) =>
    {
        
        const postBody =
        {
            id: payload, // dal payload(component postFuturi) prendo l'id del post
        }
        axios
        .post('single-post',postBody) // rolla laravel passando l'id del post
        .then(res => 
            {
                commit('setSinglePost', res.data.result.postData); //passo il post alla funzione setSinglePost
                router.push('/singlePost'); //e richiamo la rotta
            })
        .catch( (err) =>
            {
                commit("setErrorsMyPosts",err.response.data.result);
            });

    },
    
    //chiamata api che esegue l'update di un post
    updatePost: ( {commit},payload ) =>
    {
        console.log(payload);
        const formData = new FormData(); //questo formData() è utilizzato per passare l'immagine in modo giusto, utilizzando un postBody non funzionerà. SE HAI FILE USA FORMDATA
        formData.append('imgContent',payload.sentPost.photo);
        formData.append('title',payload.sentPost.title);
        formData.append('textContent',payload.sentPost.textContent);
        formData.append('dataPubblication',payload.sentPost.dataPubblication);
        formData.append('id',payload.id);
        console.log(formData);
        
        axios
        .post('update-post',formData) // rotta laravel
        .then(res => 
            {
                commit("setSinglePost",res.data.result); // passo il vecchio post aggiornato alla funzione setSinglePost
                router.push('/MyPosts'); //richiamo router e vado nella  pagina personale del singolo utente
            })
        .catch( (err) =>
            {
                if (err.response.status == 422)
                {
                    commit('setErrors', err.response.data.message);  //prendo gli errori da laravel e li inserisco nella mia collection tramite setErrors
                }
            });

    },

    //chiamata api per crearre un nuovo post
    createPost: ( {commit},payload ) =>
    {
        const formData = new FormData(); //questo formData() è utilizzato per passare l'immagine in modo giusto, utilizzando un postBody non funzionerà. SE HAI FILE USA FORMDATA
        if(payload.photo != null)
        {
           formData.append('imgContent',payload.photo);
        }

        formData.append('title',payload.title);
        formData.append('textContent',payload.textContent);
        formData.append('dataPubblication',payload.dataPubblication);

        for (var i = 0; i < payload.idUsersTagged.length; i++) 
        {
            formData.append("userTag_id["+i+" ]",payload.idUsersTagged[i]);
        }
        
        axios
        .post('create-post',formData)
        .then(res => 
        {
            commit('setPostData', res.data.result); // queste 2 non servirebbero, ma senza il commit tra graffe non funziona, il mio payload è undefined
        })
        .catch( (err) =>
        {
            if (err.response.status == 422)
            {
                commit('setErrors', err.response.data.message); //prendo gli errori da laravel e li inserisco nella mia collection tramite setErrors
            }
        });  

    },

    


};


export default {
    state,
    getters,
    mutations,
    actions
};