 import axios from 'axios';
 import router from '../../router'
const state = 
{
    
    userData:  localStorage.getItem('user'), 
    token: localStorage.getItem('token') || null, // token inizialmente sarà o null o un valore qualora l'utente era già loggato
};
const getters = 
{   //getter per prendere dallo state il singolo user
    user: state => 
    {
        return state.userData;
    },
    //getter per prendere dallo state il token
    token: state =>
    {
        return state.token;
    },
    //getter per prendere dallo state l'utente see è loggato o meno, 
    isAuth: state =>
    {
        return state.token !== null; // il risultato tornerà il token see è diverso da null
    },

    

};
const mutations = 
{
    //mutations per memorizzare l'utente utilizzata in fase di registrazione
    setUserData: ( state, payload ) => 
    {
        state.userData = payload;
    },

    // mutation per momorizzare l'utente utilizzata in fase di login, è separeta dalla mutation setUserData per poter gestire il token
    login: ( state, payload ) =>
    { 
        state.userData = payload.userData;
        localStorage.setItem('user',payload.userData);
        
        state.token = payload.token; 
        localStorage.setItem('token',payload.token); //inserire il token nello storage di vue js

        router.push('/'); 

    },

    //mutation logout che svuoterà il toke, rimuovendo anche la sua allocazione nell store di vue js, e svuoterà l'oggetto userData
    logout: (state) =>
    {
        state.userData = {};
        state.token = null;
        localStorage.removeItem('token');
        router.push('/Login');
    },
    

   
};
const actions = 
{  
    //chiamata api per registrare l'utente
    registerUser: ({commit},payload) =>
    {
        const postBody = //dal payload passato dalla view Registrare valorizzo postBody
        {   
            name: payload.name,
            email: payload.email,
            password: payload.password,
        }

        axios
        .post('register-user',postBody)  // rotta laravel
        .then( (res) =>
        {
            commit('setUserData',postBody); //creo l'oggetto user tramite la funzione setUserData
            commit('login', res.data.result);  //richiamo il login in modo da forzare l'utente ad essere loggato alla registrazione
        })
        .catch( (err) =>
        {
            if (err.response.status == 422)
            {
                commit('setErrors', err.response.data.message); //prendo gli errori da laravel e li inserisco nella mia collection tramite setErrors
            }
        });
        
    },  

    // chiamate api che esegue il login
    loginUser: ({commit}, payload) =>
    {
        axios
        .post('user-login',payload)  // rotta laravel
        .then( res =>
        {
            commit('login',res.data.result); //richiamo la funzione login passando l'utente ottenuto da laravel in modo da settare il token e uerData
        })
        .catch( err =>
        {
            if (err.response.status == 400)
            {
                commit('setErrors', err.response.data.message); //prendo gli errori da laravel e li inserisco nella mia collection tramite setErrors
            }
            else if (err.response.status == 401) 
            {
                commit('setErrors', err.response.data.message); //prendo gli errori da laravel e li inserisco nella mia collection tramite setErrors
            }
        });
    },

    //richiamato la funzione per fare una chiamata api a laravel
    logout: ({commit}) =>
    {
        axios
            .post('logout') //rotta laravel
            .then( (res) =>
            {
                if(res.status === 200) // la funzione logout restituisce uno status di 200 così da verificare se ci sono stati errore
                {
                    commit('logout'); // richiamo la funzione logout in modo da svuotare il token e l'utente attivo
                }
                else
                {
                    throw new Error('errore di Logout'); //errori se lo status è diverso da 200
                }
            })
            .catch( (err) =>
            {
                console.log(err);
            });
    },
    
};


export default {
    state,
    getters,
    mutations,
    actions
};