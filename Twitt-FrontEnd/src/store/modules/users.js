import axios from 'axios';
const state = 
{
    allUsers: {},
};

const getters = 
{
    //getter per prendere tutti gli utenti
    allUsers: state =>
    {
        return state.allUsers;
    },
   
};

const mutations =   
{
    //mutation per tutti gli utenti
    setAllUsers: ( state,payload) => // assegno al valore dello state quello della chiamata api
    {
        state.allUsers = payload;
    },
};

const actions =
{
    //chiamata api per prendere tutti i post pubblicati dal singolo utente
    getAllUsers: ( {commit} ) =>
    {
        axios
        .get('get-users') // rotta laravel
        .then(res => 
            {
                commit('setAllUsers', res.data.result); //richiamo setAllUsers passando tutti gli utenti ricevuti la laravel
            })
        .catch( (err) =>
            {
                console.log(err);
            });

    },


};


export default {
    state,
    getters,
    mutations,
    actions
};