import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import posts from './modules/posts'
import users from './modules/users'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: { //importo i moduli per le chiamate api separandole
    auth, //chiamate api relative alla registrazione, login, logout
    posts, //chiamate api relative alla modifica e visualizzazione dei post
    users // chiamata api per prendre tutti gli utenti
  }
})
