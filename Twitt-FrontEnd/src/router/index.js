import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/Registrati',
    name: 'Registrati',
    
    component: () => import( '../views/Registrati.vue'),
    
    beforeEnter: (to,from,next) =>
    {
      if( store.getters.isAuth ) 
      {
        next('/');
      }
      else
      {
        next();
      }
    }
    
  },
  {
    path: '/singlePost',
    name: 'singlePost',
    
    component: () => import( '../views/singlePost.vue'),
    
    
  },
  {
    path: '/Login',
    name: 'Login',
    
    component: () => import( '../views/Login.vue'),
    beforeEnter: (to,from,next) =>
    {
      if( store.getters.isAuth )
      {
        next('/');
      }
      else
      {
        next();
      }
    }
    
  },
  {
    path: '/',
    name: 'MainPage',
    
    component: () => import( '../views/MainPage.vue'),
    beforeEnter: (to,from,next) =>
    {
      if( store.getters.isAuth ) //controlla
      {
        next();
      }
      else
      {
        next('/Login'); //se non sei loggato vai qua 
      }
    }
    
  },
  {
    path: '/MyPosts',
    name: 'MyPosts',
    
    component: () => import( '../views/MyPosts.vue'),
    beforeEnter: (to,from,next) =>
    {
      if( store.getters.isAuth ) 
      {
        next();
      }
      else
      {
        next('/Login'); 
      }
    }
    
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
