module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  
  theme: {
    
    extend: {
        colors:
        {
          'MainLayout': '#800000',
          'MainButton': '#FF4747',
          'MainButtonHover': '#FF8F8F'
        },
        
        
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
