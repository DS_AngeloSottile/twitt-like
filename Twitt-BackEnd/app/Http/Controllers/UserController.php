<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Models\User;
use App\Models\Post;
use App\Models\PostContent;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    //Funzione per controllare tutti i post pubblicati da un singolo utente
    public function postPublished()
    {
        $user = User::findOrFail( ( Auth::id() ) ); //controllo se l'utente è effettivamente loggato


        $posts =  $user->posts()->where('dataPubblication', '<=', Carbon::now())->with(['User','postContent.users'])->get(); // prendo tutti i post pubblicati

        if(!count($posts) > 0)
        {
           return response()->json( ['result' => "posts not founded",'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY ],JsonResponse::HTTP_UNPROCESSABLE_ENTITY );
        }

        return response()->json( ['result' => $posts, 'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK );  //nvio il json
    }


    //Funzione per controllare tutti i post che verranno pubblicati da un singolo utente
    public function postFuture()
    {
        $user = User::findOrFail( ( Auth::id() ) ); //controllo se l'utente è effettivamente loggato


        $posts =  $user->posts()->where('dataPubblication', '>=', Carbon::now())->with(['User','postContent.users'])->get(); // prendo tutti i post che verranno pubblicati

        if(!count($posts) > 0)
        {
           return response()->json( ['result' => "posts not founded",'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY ],JsonResponse::HTTP_UNPROCESSABLE_ENTITY );
        }

        return response()->json( ['result' => $posts, 'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK ); //invio il json
    }


    //funzione di creazione post
    public function createPost( PostCreateRequest $request )
    {

        try
        {
            DB::beginTransaction();


            $user = User::findOrFail( ( Auth::id() ) ); //controllo se l'utente esiste, se no invio un errore

            // check sulla data
            if( $request->dataPubblication == null)
            {
                $dataPubblication = Carbon::now()->format('Y-m-d H:i'); // se non è stata inserita sostituisce con Carbon
            }
            else
            {
                $dataPubblication = $request->dataPubblication; //se è stata inserita utilizzare la variabile dalla request
            }

            $postData =
            [
                'title'                => $request->title,
                'dataPubblication'      => $dataPubblication,
                'user_id'               => $user->id,
            ]; // creo i nuovi dati del nuovo oggetto Post

            $post = Post::create($postData); // e lo creo



            $postContent = new PostContent //creo un oggetto che contiene il contenuto del post
                (

                    [
                        'post_id'       => $post->id,
                        'textContent'   => $request->textContent,
                        'musicContent'  => $request->musicContent,
                    ]

                );



            if($request->hasFile( 'imgContent' )) //se ho un'immagine la inserisco nel contenuto del post
            {
                $file = $request->file( 'imgContent' );
                $filename = time(). '.' . $file->getClientOriginalExtension(); // mette un nome generico al file
                $file_path = 'postContent/' . $postContent->id . '/imgContent';
                $uploaded_file = $file->storeAs( $file_path,$filename,'public');
                $postContent->imgContent = $uploaded_file; // php artisan storage:link
            }
            $post->postContent()->save($postContent); // creo il contenuto


            if($request->has('userTag_id')) //se è stato taggato qualcuno lo inserisco
            {
                $postContent->users()->sync($request->userTag_id);
            }

            $user->posts()->save($post); // lo inserisco nel database come oggetto relazionato di user


            $finalPost = $post->with('postContent.users')->find($post->id);

            $result =
            [
                'postData' => $finalPost,
            ]; //creo il file per il json

            DB::commit();
            return response()->json( ['result' => $result,'code' => JsonResponse::HTTP_CREATED ],JsonResponse::HTTP_CREATED ); // invio il json

        }
        catch(\Exception $e) //se la transazione avrà un errore non verrano eseguite le query
        {
            Db::rollBack();
            return response()->json( //json di errore in caso di "malfunzionamento"
            [
                'error' => 'Errore nella connessione al database',
                'status' =>JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            ],
            JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    //funzione per eseguire un update su un singolo post
    public function updatePost(PostUpdateRequest $request)
    {

        try
        {
            DB::beginTransaction();

            $user = User::findOrFail( ( Auth::id() ) ); //controllo se lo user esiste

            $id =  $request->id; // id del post inviato tramite la request
            $post =  $user->posts->find($id); // controllo se l'id è valido

            if(!$post)  // se non è valido invio un json d'errore
            {
                return response()->json( ['result' => "post non trovato",'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY ],JsonResponse::HTTP_UNPROCESSABLE_ENTITY );
            }
            else
            {
                // se il post esiste faccio un check sulla data di pubblicazione per vedere se l'utente ha scelto di inserirla o meno
                if( $request->dataPubblication == null)
                {
                    $dataPubblication = Carbon::now()->format('Y-m-d H:i');
                }
                else
                {
                   $dataPubblication = $request->dataPubblication;
                }

                //check sulla request se è stato passato un file
                if($request->hasFile( 'imgContent' ))
                {
                    $file = $request->file( 'imgContent' );

                    $filename = time(). '.' . $file->getClientOriginalExtension(); // mette un nome generico al file
                    $file_path = 'postContent/' . $post->postContent->id . '/imgContent';
                    $uploaded_file = $file->storeAs( $file_path,$filename,'public');

                    $post->postContent->imgContent = $uploaded_file;
                }
            }

            // ??
            $post->title = $request->title;
            $post->dataPubblication = $dataPubblication;

            $post->postContent->textContent = $request->textContent;
            $post->postContent->musicContent = $request->musicContent;


            $post->postContent->save();
            $post->save();
            //??

            $result =
            [
                'postData' => $post,
            ]; // creo l'array da passare nel json

            DB::commit();
            return response()->json( ['result' => $result,'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK ); // invio il json
        }

        catch(\Exception $e) //se la transazione avrà un errore non verrano eseguite le query
        {
            Db::rollBack();
            return response()->json( //json di errore in caso di "malfunzionamento"
            [
                'error' => 'Errore nella connessione al database',
                'status' =>JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            ],
            JsonResponse::HTTP_INTERNAL_SERVER_ERROR);

        }

    }

    //funzione per prendere il singolo post
    public function singlePost(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $id =  $request->id; // prendo dalla requer l'id del post

            $user = User::findOrFail( ( Auth::id() ) ); //controllo se esiste un utente loggato

            $post =  $user->posts()->with('postContent.users')->find($id); // prendo il post tramite l'id

            if(!$post) // se non esiste invio un json d'errore
            {
                return response()->json( ['result' => "post non trovato",'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY ],JsonResponse::HTTP_UNPROCESSABLE_ENTITY );
            }

            $result =
            [
                'postData' => $post,
            ];

           // DB::commit();
            return response()->json( ['result' => $result,'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK );


        }
        catch(\Exception $e) //se la transazione avrà un errore non verrano eseguite le query
        {
            Db::rollBack();
            return response()->json( //json di errore in caso di "malfunzionamento"
            [
                'error' => 'Errore nella connessione al database',
                'status' =>JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            ],
            JsonResponse::HTTP_INTERNAL_SERVER_ERROR);

        }

    }


    //funzione per prendere tutti gli utenti
    public function getAllUsers()
    {
        $users = User::all(); //query per prendere tutti gli utenti


        return response()->json( ['result' => $users,'code' => JsonResponse::HTTP_OK ],JsonResponse::HTTP_OK ); // restituisco il json passando gli utenti

    }


}
