<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //funzione di registrazione utente
    public function registerUser( Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'name'              => 'required|max:255',
                'email'             => 'required|max:255|email|unique:users,email',
                'password'          => 'required|max:255|min:8|alpha_num',
            ]
        );


        if($validator->fails())
        {
            return response()->json( ['error' => true, 'message' => $validator->errors(), 'status' =>JsonResponse::HTTP_UNPROCESSABLE_ENTITY ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }


        try //inizio transazione
        {
            DB::beginTransaction(); //inizio transazione


            $input_data =
            [
                'name'                  =>$request->name,
                'email'                 =>$request->email,
                'password'              =>Hash::make($request->password),
            ]; //creo l'array utente dalla request

            $user = User::create($input_data); //creo l'oggetto utente con l'array

            $result =
            [
                'token'         => $user->createToken('token')->plainTextToken, //registro il token
                'userData'      => $user,
            ]; //creo l'array da inviare come json


            DB::commit();
            return response()->json( ['result' => $result,'code' => JsonResponse::HTTP_CREATED ],JsonResponse::HTTP_CREATED ); //invio il json
        }
        catch(\Exception $e)  //se la transazione avrà un errore non verrano eseguite le query
        {
            Db::rollBack();
            return response()->json( //json di errore in caso di "malfunzionamento"
            [
                'error' => 'Errore nella connessione al database',
                'message' => $validator->errors(),
                'status' =>JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            ],
            JsonResponse::HTTP_INTERNAL_SERVER_ERROR);

        }

    }

    //funzione di login utente
    public function userLogin( Request $request )
    {
        $validator = Validator::make($request->all(),
            [
                'email'             => 'required|email',
                'password'          => 'required',
            ]
        );


        if($validator->fails())
        {
            return response()->json(
                [
                    'error' => true,
                    'message' => $validator->errors(),
                    'status' =>JsonResponse::HTTP_BAD_REQUEST,
                ],
                JsonResponse::HTTP_BAD_REQUEST,
            );
        }


        if( Auth::attempt( ['email' => $request->email, 'password' => $request->password ] ) ) //con il metodo attempt eseguo una funzione di login
        {
            $user = Auth::user(); //prendo il mio user solo se è stato loggato

            $result =
            [
                'token'         => $user->createToken('token')->plainTextToken, //gli creo il suo token
                'userData'      => $user,
            ]; //invio l'array da convertire in json

            return response()->json( // invio il json
                [
                    'result' => $result,
                    'code' =>JsonResponse::HTTP_OK,
                ],
                JsonResponse::HTTP_OK,
            );

        }
        else
        {
            $result =
            [
                'message' =>
                [
                    0 => 'Login error'
                ]
            ];
            return response()->json( //invio un json d'errore
                [
                    'error' => true,
                    'message' => $result,
                    'status' =>JsonResponse::HTTP_UNAUTHORIZED,
                ],
                JsonResponse::HTTP_UNAUTHORIZED,
            );

        }

    }

    //funzione di logout
    public function logout( Request $request)
    {
        Auth::user()->tokens()->delete(); //il token verrà eliminato dal database
    }
}
