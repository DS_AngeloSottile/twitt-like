<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;//HasApiTokens per eseguire operazioni di registrazione login e logout con i token

class User extends Authenticatable
{
    use Notifiable, HasApiTokens; //HasApiTokens per eseguire operazioni di registrazione login e logout con i token
    //campi User database
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =
    [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //relazione  User  1 a n  Post
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }


    public function postContents() //relazione n a n postContent
    {
        return $this->belongsToMany('App\Models\User','post_content_user','user_id','post_content_id');
    }
}
