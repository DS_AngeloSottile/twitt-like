<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //campi del database Post
    protected $fillable =
    [
        'title',
        'dataPubblication',
        'user_id',
    ];

    //relazione 1 a n invers   Post User
    public function User()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function postContent() //relazione 1 a 1 postContent
    {
        return $this->hasOne('App\Models\PostContent');
    }
}
