<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostContent extends Model
{
    protected $fillable =
    [
        'post_id',
        'textContent',
        'imgContent',
        'musicContent'
    ];

    protected $appends =
    [
        'postContent_image_full_url',
    ];

    public function getPostContentImageFullUrlAttribute()
    {
         return 'http://localhost:8000/' . 'storage/' . $this->imgContent;
    }

    public function post() //relazione 1 a 1 post
    {
        return $this->belongsTo('App\Models\Post');
    }

    public function users() //relazione n a n users
    {
        return $this->belongsToMany('App\Models\User','post_content_user','post_content_id','user_id');
    }
}
