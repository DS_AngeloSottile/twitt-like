<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group( ['middleware' => 'api' ], function()
{ //             url
    Route::get('posts','PostsController@index'); // le api devi richiamarle con PostMan http://localhost:8000/api/users devi riscriverti l'URL
    Route::get('get-users','UserController@getAllUsers');
} );

Route::group( ['middleware' => 'api' ], function()
{
    Route::post('register-user','AuthController@registerUser');
    Route::post('user-login','AuthController@userLogin');
} );

Route::group( ['middleware' => 'auth:api' ], function()
{
    Route::post('my-postPublished','UserController@postPublished');
    Route::post('logout','AuthController@logout');
    Route::post('my-postFuture','UserController@postFuture');
    Route::post('create-post','UserController@createPost');
    Route::post('update-post','UserController@updatePost');
    Route::post('single-post','UserController@singlePost');
} );
