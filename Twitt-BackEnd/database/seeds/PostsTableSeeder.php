<?php

use App\Models\Post;
use App\Models\PostContent;
use App\Models\User;
use Illuminate\Database\Seeder;


use Carbon\Carbon;
use Illuminate\Support\Arr;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $usersArray = User::get()->pluck('id')->toArray();

        foreach($users as $user)
        {
            for($i = 0; $i < 4; $i++)
            {
                $postData =
                [
                    'title'                =>'TitoloPost',
                    'dataPubblication'     => Carbon::createFromFormat('Y-m-d H', '1975-05-21 22')->toDateTimeString(),
                    'user_id'               => $user->id,
                ];

                $post = Post::create($postData);


                $postContent = new PostContent
                (

                    [
                        'post_id' => $post->id,
                        'textContent' => 'Contenuto testo Seeder',
                        'imgContent' =>'',
                        'musicContent' => '',
                    ]

                );
                $post->postContent()->save($postContent);


                $postContentUsers = Arr::random($usersArray,2);
                $postContent->users()->sync($postContentUsers);


                $user->Posts()->save($post);
            }

        }
    }
}
