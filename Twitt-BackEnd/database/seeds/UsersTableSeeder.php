<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('it_IT');

        for($i = 0; $i < 10; $i++)
        {
            $user_data =
            [
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail(),
                'password' => Hash::make('password'),
            ];

            User::Create($user_data);

        }


    }
}
